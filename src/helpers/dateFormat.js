import moment from 'moment';

export const getHourseFromSeconds = (sec, format) => { // correct only for one day (24H)
    return moment.utc(sec*1000).format(format);
};

export const formatTime = (val) => {
    let id = val.indexOf(':'),
        res = '';
    if( id !== -1 ) {
        return res += val.slice(0, id) + 'h ' + val.slice(id+1) + 'm';
    }
};

export const getSecondsFromHourse = (val, isDate) => {
    let hourse = isDate ? moment(val).format("HH:mm:ss") : moment(val)._i;
    return moment(hourse, 'HH:mm:ss: A').diff(moment().startOf('day'), 'seconds');
};

export const getTimeFromSeconds = (sec, {h = '', sep = ':', m = '', isMin = true}) => { // use for getting hours from seconds, if seconds more then 86400 (24h)
    let hours = Math.trunc(sec/3600);
    let minutes = Math.trunc(sec/60) % 60;
    if(minutes < 10) {
        minutes += '0';
    }
    return isMin ? (hours + h + sep + minutes + m) : (hours + h);
};

export const getQuarterTimeFromSeconds = (sec) => {
    let hours = Math.floor(sec/60*100/60/100);
    let minutes = Math.trunc(sec/60%60)*100/60;

    if(minutes < 10) {
        minutes += '0';
    }
    if(hours < 10) {
        hours = '0' + hours;
    }
    return hours + "." + minutes;
};

export const getCurDateAndTimeFromSec = (curDay, sec) => {
    return moment(curDay).add(sec, 'seconds').format('YYYY-MM-DD HH:mm:ss');
};
