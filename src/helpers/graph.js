// import {getHourseFromSeconds} from '@/helpers/dateFormat';
// import moment from 'moment';

const DURATION_RECT_WIDTH = 160;

let durationBlockAttr = null,
    getTimeBlockAttr = null,
    getDragBlockAttr = {};

const setAttributes = (el, attr) => {
    for(let key in attr) {
        el.setAttributeNS(null, key, attr[key]);
    }
};

const getRow = (attrs, maxLength, stepY = 0) => {
    let y = 0;
    let line = document.createElementNS(attrs.ns, 'line');
    y += stepY;

    let attr = {x1: 0, y1: y, x2: maxLength, y2: y, style: attrs.styles };
    
    setAttributes(line, attr);

    return line;
};

const getCols = (attrs, maxHeight, stepX = 0) => {
    let x = 0;
    let line = document.createElementNS(attrs.ns, 'line');
    x += stepX;

    let attr = {x1: x, y1: 0, x2: x, y2: maxHeight, style: attrs.styles};

    setAttributes(line, attr);
    
    return line;
};

const getMinutes = (attrs, stepX = 0, stepY = 0, n=0, k=0) => {
    let x = 0;
    let y = 0;
    x += stepX;
    y += stepY;

    let g = document.createElementNS(attrs.ns, 'g');
    let line1 = document.createElementNS(attrs.ns, 'line');
    let line2 = document.createElementNS(attrs.ns, 'line');
    let line3 = document.createElementNS(attrs.ns, 'line');

    let attr1 = {x1: (x+8), y1: y, x2: (x+8), y2: (y+n), style: attrs.styles };
    let attr2 = {x1: (x+15), y1: y, x2: (x+15), y2: (y+k), style: attrs.styles };
    let attr3 = {x1: (x+22), y1: y, x2: (x+22), y2: (y+n), style: attrs.styles };

    setAttributes(line1, attr1);
    setAttributes(line2, attr2);
    setAttributes(line3, attr3);
    
    g.append(line1);
    g.append(line2);
    g.append(line3);
    
    return g;
};

const getCoords = (length, sec) => {
    return (length / 86400) * sec;
};

const getSecFromPx = (length, px) => {
    return (px * 86400)/length;   
};

const getDurationBlock = (attr) => {
    durationBlockAttr = attr;
    let durationRect = document.createElementNS(attr.ns, 'rect'),
        durationText = document.createElementNS(attr.ns, 'text');
    
    setAttributes(durationText, {x: attr.rectX2+18, y:-13, class: 'cur-areas__duration-txt', id: 'txt'});
    setAttributes(durationRect, {x: attr.rectX2+18, y:-33, width: DURATION_RECT_WIDTH, height: 28, class: 'cur-areas__duration'});
    
    durationText.innerHTML = "Duration: "
    durationText.innerHTML += attr.durationTime;

    getRepositionDurationBlock(durationRect, durationText, attr);

    return { rect: durationRect, txt: durationText };
};

const getTimeBlock = (attr) => {
    getTimeBlockAttr = attr;
    let startTimeText = document.createElementNS(attr.ns, 'text'),
    endTimeText = document.createElementNS(attr.ns, 'text'),
    startTimeRect = document.createElementNS(attr.ns, 'polygon'),
    endTimeRect = document.createElementNS(attr.ns, 'polygon');

    // console.log(">>> attrs", attr)
    
    setAttributes(startTimeRect, 
        {
            points: `${attr.rectX1},${attr.rectY1} ${attr.x1},${attr.rectY1} ${attr.x1},0  ${attr.x1-7},-5 ${attr.rectX1},-5`,
            class: 'cur-areas__rect cur-areas_start-time'
        });
    setAttributes(endTimeRect, 
        {
            points: `${attr.x2},${attr.rectY1} ${attr.rectX2},${attr.rectY1} ${attr.rectX2},-5 ${attr.x2+7},-5 ${attr.x2},0 `, 
            class: 'cur-areas__rect cur-areas_end-time'
        });
        
    setAttributes(startTimeText, {x: attr.rectX1, y: 0, class: 'cur-areas__text cur-areas_start-time-txt'});
    setAttributes(endTimeText, {x: attr.x2, y: 0, class: 'cur-areas__text cur-areas_end-time-txt'})
        
    startTimeText.innerHTML = attr.startTime;            
    endTimeText.innerHTML = attr.endTime;

    return { startTimeRect: startTimeRect, endTimeRect: endTimeRect, startTimeText: startTimeText, endTimeText: endTimeText }
};

const getDraggableBlock = (attr) => {    
    let leftBlock = document.createElementNS(attr.ns, 'polygon'),
        rightBlock = document.createElementNS(attr.ns, 'polygon'),
        blockWidth = 35,
        blockHeight = 28;

    getDragBlockAttr = {...attr, blockWidth: blockWidth, blockHeight: blockHeight};

    setAttributes(rightBlock,
        {
            points: `${attr.x2},${attr.y} ${attr.x2+blockWidth},${attr.y} ${attr.x2+blockWidth+10},${attr.y+blockHeight/2} ${attr.x2+blockWidth},${attr.y+blockHeight} ${attr.x2},${attr.y+blockHeight} `, 
            class: 'cur-areas__rect cur-areas__rect-drag-right draggable'
        });
    setAttributes(leftBlock,
        {
            points: `${attr.x1},${attr.y} ${attr.x1-blockWidth},${attr.y} ${attr.x1-blockWidth-10},${attr.y+blockHeight/2} ${attr.x1-blockWidth},${attr.y+blockHeight} ${attr.x1},${attr.y+blockHeight} `, 
            class: 'cur-areas__rect cur-areas__rect-drag-left'
        });
    
    return {rightBlock: rightBlock, leftBlock: leftBlock}

};

const getViolantion = (attrs, type, x, y) => {
    let g = document.createElementNS(attrs.ns, 'g'),
        // triangle = document.createElementNS(attrs.ns, 'polygon'),
        circle = document.createElementNS(attrs.ns, 'circle'),
        triangleText = document.createElementNS(attrs.ns, 'text'),
        typeEl = (type.indexOf('-') <= -1) ? type : 'v';
        

    // setAttributes(triangle, {points: `${x},${y} ${x+11},${y+18} ${x-11},${y+18}`, class: attrs.classIco, 'data-type': type, 'pointer-events': 'visibleFill'});
    setAttributes(circle, {cx: x, cy: y+9, r: 9, class: attrs.classIco, 'data-type': type, 'pointer-events': 'visibleFill'});
    setAttributes(triangleText, {x: x-3, y: y+12, class: attrs.classTxt + typeEl});

    triangleText.innerHTML = typeEl;

    g.append(circle);
    g.append(triangleText);
    
    return g;
};

const getSvgElMiddlePosition = (el1, el2) => {
    let w1 = el1.getBBox().width; 
    let w2 = el2.getBBox().width;
    let x = el1.getBBox().x;
    x += (w2 - w1)/2;

    setAttributes(el1, {x: x});
};

const getRepositionDurationBlock = (el_block, el_txt, attr, deltha) => {
    let curDelthaX1 = deltha ? deltha.x1 : 0,
        curDelthaX2 = deltha ? deltha.x2 : 0,
        x1 = attr.x1+ curDelthaX2,
        x2 = attr.x2 + curDelthaX2;
    
        if(attr.gridLength - (x2 + 10) < DURATION_RECT_WIDTH) {  // reposition Duration Block
            if(x2 - x1 > DURATION_RECT_WIDTH) {
            let middle = (x2-x1-DURATION_RECT_WIDTH)/2;
            setAttributes(el_block, {x: x1+middle});
            setAttributes(el_txt, {x: x1+middle});
        }
        if(curDelthaX2) {
            setAttributes(el_block, {x: attr.rectX1+curDelthaX2-DURATION_RECT_WIDTH + 50});
            setAttributes(el_txt, {x: attr.rectX1+curDelthaX2-DURATION_RECT_WIDTH + 50});
            
            if(curDelthaX2 - curDelthaX1 <= DURATION_RECT_WIDTH+14) {
                setAttributes(el_block, {x: curDelthaX1-(DURATION_RECT_WIDTH+getTimeBlockAttr.rectX2+10)});
                setAttributes(el_txt, {x: curDelthaX1-(DURATION_RECT_WIDTH+getTimeBlockAttr.rectX2+10)});
            }
        }
        else {
            setAttributes(el_block, {x: attr.rectX1-(DURATION_RECT_WIDTH+10)});
            setAttributes(el_txt, {x: attr.rectX1-(DURATION_RECT_WIDTH+10)});
        }
    }
};

const updateDurationAttributes = (el_block, el_txt, curTime, deltha) => {
    el_txt.innerHTML = "Duration: ";
    el_txt.innerHTML += curTime;
    setAttributes(el_block, {x: durationBlockAttr.rectX2 + deltha.x2 +10});
    setAttributes(el_txt, {x: durationBlockAttr.rectX2 + deltha.x2+10});

    getRepositionDurationBlock(el_block, el_txt, durationBlockAttr, deltha);
    getSvgElMiddlePosition(el_txt, el_block);
};

const updateEndTimeBlockAttributes = (el_block, el_txt, deltha, curTime, step) => {
    el_txt.innerHTML = curTime;
    setAttributes(el_block, {
        points: `${deltha},${getTimeBlockAttr.rectY1} ${getTimeBlockAttr.rectX2+deltha-step},${getTimeBlockAttr.rectY1} 
                 ${getTimeBlockAttr.rectX2+deltha-step},-5 ${deltha+7},-5 ${deltha},0 `
    });

    setAttributes(el_txt, {x: deltha});
};

const updateStartTimeBlockAttributes = (el_block, el_txt, deltha, curTime) => {
    el_txt.innerHTML = curTime;
    setAttributes(el_block, {
        points: `${getTimeBlockAttr.rectX1+deltha},${getTimeBlockAttr.rectY1} ${getTimeBlockAttr.x1+deltha},${getTimeBlockAttr.rectY1} 
                 ${getTimeBlockAttr.x1+deltha},0  ${getTimeBlockAttr.x1-7+deltha},-5 ${getTimeBlockAttr.rectX1+deltha},-5`,
    });

    setAttributes(el_txt, {x: getTimeBlockAttr.rectX1+deltha});
};

const updatedDagRightElAttributes = (el, deltha) => {
    setAttributes(el, {
        points: 
            `${deltha},${getDragBlockAttr.y} ${getDragBlockAttr.blockWidth+deltha},${getDragBlockAttr.y} 
             ${getDragBlockAttr.blockWidth+10+deltha},${getDragBlockAttr.y+getDragBlockAttr.blockHeight/2} 
             ${getDragBlockAttr.blockWidth+deltha},${getDragBlockAttr.y+getDragBlockAttr.blockHeight} 
             ${deltha},${getDragBlockAttr.y+getDragBlockAttr.blockHeight}`
    });
};

const updatedDagLeftElAttributes = (el, deltha) => {
    setAttributes(el, {
        points: 
            `${deltha},${getDragBlockAttr.y} ${deltha-getDragBlockAttr.blockWidth},${getDragBlockAttr.y} 
             ${deltha-getDragBlockAttr.blockWidth-10},${getDragBlockAttr.y+getDragBlockAttr.blockHeight/2} 
             ${deltha-getDragBlockAttr.blockWidth},${getDragBlockAttr.y+getDragBlockAttr.blockHeight} 
             ${deltha},${getDragBlockAttr.y+getDragBlockAttr.blockHeight} `, 
    });
};

const getStatusesDuration = (data, curDate, createdDay, secondsInDay) => {
    let time = 0;
        for(let i=0; i<data.length; i++) {                    
            if(i>0) {
                time = data[i].created_at - data[i-1].created_at;
                data[i-1].duration = time;
            }
            if(i>=data.length-1) {
                time = secondsInDay - data[i].created_at;
                data[i].duration = time;
            }
            if(createdDay < curDate && data<=1) {
                time += secondsInDay - (secondsInDay - data[i].created_at)
                data[i].duration = time;
                data[i].created_at = 0;
            }
        }   
};


export default {
    setAttributes,
    getRow,
    getCols,
    getMinutes,
    getCoords,
    getSecFromPx,
    getDurationBlock,
    getTimeBlock,
    getDraggableBlock,
    getViolantion,
    getSvgElMiddlePosition,
    updateDurationAttributes,
    updateStartTimeBlockAttributes,
    updateEndTimeBlockAttributes,
    updatedDagRightElAttributes,
    updatedDagLeftElAttributes,
    getStatusesDuration
}