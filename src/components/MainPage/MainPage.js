import moment from 'moment';
import logbook from '@/helpers/logbook';
import {getHourseFromSeconds, getSecondsFromHourse, formatTime, getCurDateAndTimeFromSec} from '@/helpers/dateFormat';
import Graph from '../Graph/Graph.vue';
import MaskedInput from 'vue-masked-input';

export default {
  name: 'MainPage',
  components: {
    Graph, MaskedInput
  },
  data: () => ({
    graphData: [
       {
          "status": "off",
          "dateTime": "2019-11-06 00:00:00"
      },
      {
          "status": "dr",
          "dateTime": "2019-11-06 05:00:00"
      },
      {
          "status": "sb",
          "dateTime": "2019-11-06 07:00:00"
      },
      {
          "status": "dr",
          "dateTime": "2019-11-06 09:00:00"
      },
      {
          "status": "on",
          "dateTime": "2019-11-06 11:00:00"
      },
      {
          "status": "off",
          "dateTime": "2019-11-06 20:00:00"
      }
    ],
    idx: null,
    statusList: [],
    statusNodes: null,
    secondsInDay: 86400,
    curDate: '2019-11-06',
    isGraphEdit: false,
    editedStatus: 'off',
    pickedTimeFrom: 0,
    pickedTimeTo: '',
    log_increment: 15,
    updatedStatusList: null,
    statusListForRender: null,
    isSave: false,
    statusListBeforeEdit: null,
    statusNodesBeforeEdit: null,
  }),
  

  created() {
    this.getStatusesData;
    this.statusListForRender = this.statusList;
    logbook.getStatusesDuration(this.statusListForRender, this.secondsInDay);
    this.pickedTimeFrom = getHourseFromSeconds(this.pickedTimeFrom,'HH:mm');
    this.pickedTimeTo = `00:${this.log_increment}`;
    
  },
  
  mounted() {
    this.statusNodes = document.querySelectorAll('.logs-row');
  },

  updated() {
    if(this.log_increment > 1 && this.isGraphEdit) {
      document.querySelector("input[name=input-time_from]").addEventListener("blur", () => {
          this.roundingMinutesTo15();
      });

      document.querySelector("input[name=input-time_to]").addEventListener("blur", () => {
          this.roundingMinutesTo15();
      });
  }

    this.getCorrectTime();
    this.statusNodes = [...document.querySelectorAll('.logs-row')];
  },

  watch: {
    updatedStatusList(val) {
      if(val) {        
        this.statusListForRender = [...val];

        logbook.getStatusesDuration(this.statusListForRender, this.curDate, moment(this.graphData[0].created_at).format('YYYY-MM-DD'), this.secondsInDay); //??
        this.getDrivingStatusesTime;
      }
      else {
        this.statusListForRender = this.statusList;
      }
    },
    
    statusListForRender() {
    }
  },

  computed: {
    getStatusesData() {
      for(let i=0; i<this.graphData.length; i++) {     
          this.statusList.push({
              status: this.graphData[i].status,
              created_at: getSecondsFromHourse(this.graphData[i].dateTime, true),
              duration: 0
          });
      }            
    },

    getTimeFormatA() {
      return (val) => {
          return getHourseFromSeconds(val,'hh:mm A');
      }
    },

    getConvertedDuration() {
      return (val) => {
          return (val < this.secondsInDay) 
                  ? formatTime(getHourseFromSeconds(val, 'HH:mm')) 
                  : formatTime(getHourseFromSeconds(val, 'kk:mm'));
      }
    },

    getCurStatus() {
      return this.editedStatus;
    },

    getStartEndStatusTime() {
      let x1 = getSecondsFromHourse(this.pickedTimeFrom, false),
          x2 = getSecondsFromHourse(this.pickedTimeTo, false),
          obj = null;

      if(!isNaN(x1) && !isNaN(x2)) {
          if(x1 >= x2-this.log_increment*60) {
              x1 = x2-this.log_increment*60;
          }
          obj = {
              x1: x1, 
              x2: x2
          };                 
      }
      return obj;
    }

  },

  methods: {
    getCurStatusData(event) {            
      for(let i=0; i<this.statusNodes.length; i++) {
          this.statusNodes[i].classList.remove('active');
      }
      let elem = event.target.closest('.logs-row');
      this.idx = elem.dataset.idx;
    },

    getCurStatusItem(idx) {      
      for(let i=0; i<this.statusNodes.length; i++) {
        this.statusNodes[i].classList.remove('active');
      }
      this.statusNodes[idx].classList.add('active');
    },

    clearCurStatusData() {
      this.idx = null;
    },

    getGraphEdit() {
      this.statusListBeforeEdit = [...this.statusListForRender];
      this.statusNodesBeforeEdit = [...this.statusNodes];
      this.editedStatus = 'off';
      this.isGraphEdit = true;
    },

    getEditedStatus(event) {
      for(let i=0; i<document.querySelectorAll('.log-page-container__edit-graph-btn').length; i++) {
          document.querySelectorAll('.log-page-container__edit-graph-btn')[i].classList.remove('log-page-container__edit-graph-btn_active');
      }

      if(event.target.closest('.log-page-container__edit-graph-btn')) {
          this.editedStatus = event.target.dataset.type;
          event.target.classList.add('log-page-container__edit-graph-btn_active');
      }
    },

    saveDutyStatus() {
      this.isSave = true;
      this.isGraphEdit = false;
      this.graphData.length = 0;

      this.pickedTimeFrom = getHourseFromSeconds(0,'HH:mm');
      this.pickedTimeTo = `00:${this.log_increment}`;
      
      for(let i=0; i< this.statusListForRender.length; i++) {
        this.graphData[i] = {
          status: this.statusListForRender[i].status,
          dateTime: getCurDateAndTimeFromSec(this.curDate, this.statusListForRender[i].created_at)
        }
      }
    },

    resetGraph() {
      this.isGraphEdit = false;
      this.editedStatus = 'off';
      this.statusListForRender = [...this.statusListBeforeEdit];
      this.statusNodes = [...this.statusNodesBeforeEdit];
      this.pickedTimeFrom = getHourseFromSeconds(0,'HH:mm');
      this.pickedTimeTo = `00:${this.log_increment}`;
    },

    changeTime(el, val) {
      let secFrom = getSecondsFromHourse(this.pickedTimeFrom, false);
      let secTo = getSecondsFromHourse(this.pickedTimeTo, false);
      
      if(el == 'to') {
          secTo += val;
          if(secTo >= this.secondsInDay) {
              secTo = this.secondsInDay;
          }
          if(secTo <= this.log_increment*60) {
              secTo = this.log_increment*60;
          }
          if(secTo - secFrom < this.log_increment*60) {
              return secTo
          }           
      }
      if(el == 'from') {
          secFrom += val;

          if(secFrom <= 0) {
              secFrom = 0;
          }
      }
      
      this.pickedTimeFrom = getHourseFromSeconds(secFrom,'HH:mm');
      this.pickedTimeTo = secTo >= this.secondsInDay ? getHourseFromSeconds(secTo,'kk:mm') : getHourseFromSeconds(secTo,'HH:mm');
    },

    getCorrectTime() {
      let secFrom = getSecondsFromHourse(this.pickedTimeFrom, false),
          secTo = getSecondsFromHourse(this.pickedTimeTo, false),
          idx = this.pickedTimeFrom.indexOf(':'),
          hoursFrom = this.pickedTimeFrom.slice(0,idx),
          minutesFrom = this.pickedTimeFrom.slice(idx+1),
          hoursTo = this.pickedTimeTo.slice(0,idx),
          minutesTo = this.pickedTimeTo.slice(idx+1);                

      
      if(hoursFrom > 24 ||( hoursFrom == 24 && minutesFrom >= 1)) {
          this.pickedTimeFrom = getHourseFromSeconds(this.secondsInDay - this.log_increment*60,'HH:mm');
      }
      if(hoursTo >= 24 ||( hoursTo == 24 && minutesTo >= 1)) {              
          this.pickedTimeTo = getHourseFromSeconds(this.secondsInDay,'kk:mm');
      }
      
      if(secFrom <= 0) {
          secFrom = 0;
          this.pickedTimeFrom = getHourseFromSeconds(secFrom,'HH:mm');
      }
      if(secFrom >= (secTo - this.log_increment*60)) {
          secFrom = secTo - this.log_increment*60;
          this.pickedTimeFrom = getHourseFromSeconds(secFrom,'HH:mm');             
      }
      
      if(secTo <= 900) {
          secTo = 900;
          this.pickedTimeTo =  getHourseFromSeconds(secTo,'HH:mm');
      }
      
      if(minutesFrom >= 60) {
          hoursFrom = Number(hoursFrom) + 1;
          secFrom = hoursFrom * 3600;
          this.pickedTimeFrom = getHourseFromSeconds(secFrom,'HH:mm');
      }

      if(minutesTo >= 60) {
          hoursTo = Number(hoursTo) + 1;
          secTo = hoursTo * 3600;
          this.pickedTimeTo =  getHourseFromSeconds(secTo,'HH:mm');            
      }            
    },

    roundingMinutesTo15() {
      let idx = this.pickedTimeFrom.indexOf(':'),
          hoursFrom = this.pickedTimeFrom.slice(0,idx),
          minutesFrom = this.pickedTimeFrom.slice(idx+1),
          hoursTo = this.pickedTimeTo.slice(0,idx),
          minutesTo = this.pickedTimeTo.slice(idx+1);

      if(minutesFrom  % this.log_increment != 0) {
          if(minutesFrom.indexOf('_') == -1) {
              minutesFrom = Math.trunc(minutesFrom/this.log_increment%this.log_increment)*this.log_increment;                    
              minutesFrom = minutesFrom == 0 ? '00' : minutesFrom;
              this.pickedTimeFrom = `${hoursFrom}:${minutesFrom}`;
          }
      }

      if(minutesTo  % this.log_increment != 0) {
          if(minutesTo.indexOf('_') == -1) {
              minutesTo = Math.trunc(minutesTo/this.log_increment%this.log_increment)*this.log_increment;                    
              minutesTo = minutesTo == 0 ? '00' : minutesTo;
              this.pickedTimeTo = `${hoursTo}:${minutesTo}`;
          }
      }
    },

    updateTimeInterval(time) {
      let format = time.x2 >= this.secondsInDay ? 'kk:mm' : 'HH:mm';
      
      this.pickedTimeFrom = getHourseFromSeconds(time.x1,'HH:mm');
      this.pickedTimeTo = getHourseFromSeconds(time.x2, format);
    },

    updateStatusList(arr) {
      this.updatedStatusList = arr;
    },

    resetFlag() {
      this.isSave = false;
    }
  }
}


