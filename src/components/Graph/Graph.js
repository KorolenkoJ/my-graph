import graph from "../../helpers/graph";
import {getHourseFromSeconds, getSecondsFromHourse} from '@/helpers/dateFormat';
import logbook from '@/helpers/logbook';

export default {
  name: 'Graph',
  props: [
    'graphData', 'hoveredStatus', 'isGraphEdit', 'editedStatus', 'pickedTime', 'isSave'
  ],

  data: () => ({
    svg: null,
    path: null,
    ns: 'http://www.w3.org/2000/svg',
    common_styles: 'stroke: #8C9198;',
    cell: 30,
    rows: 5,
    cols: 25,
    gridStep: 15,
    timeRectWidth: 60,
    timeRectHeight: 28,
    secondsInDay: 86400,
    pathAttr: '',
    totalStatusTime: 0,
    drivingStatus: {},
    drivingStatusTotal: [],
    curAreasCoords: [],
    timeLables: ['M', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,'N', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 'M'],
    statusLabels: [{name: 'off', val: 0}, {name: 'sb', val: 0}, {name: 'dr', val: 0}, {name: 'on', val: 0}],
    statusesForRender: [{name: 'off', val: 0}, {name: 'sb', val: 0}, {name: 'dr', val: 0}, {name: 'on', val: 0}],
    curIdx: null,
    graphStep: 0,
    curRight_X: 0,
    curLeft_X: 0,
    editPathData: {x1: 0, x2: 0, y: 0},
    editGraphData: {x1: 0, x2: 0, y: 0},
    editedAreaWidth: 0,
    step: 900,
    isPress: false,
    direction: true,
    pathEditAttr: '',
    curArrItemRightIdx: 0,
    curArrItemLeftIdx: 0,
    curArrItemY: 0,
    editedGraphArr: [],
  }),

  created() {
    this.getDrivingStatus;
    this.graphStep = graph.getCoords(this.getGridLength, this.step);
    this.curRight_X = this.graphStep;
    this.editPathData.x2 = this.graphStep;
  },

  mounted() {
    this.svg = document.getElementById('logBook');
    this.path = document.querySelector('#logBook path');
    this.path.before(this.getGrid());

    this.curAreas = document.querySelectorAll('.cur-areas'); // Collection of current cycles areas

    this.svg.addEventListener("mouseleave", () => {
        this.curIdx = null;
    });

    this.svg.addEventListener('mousedown', (event) => {
      this.isPress = true;                
      if(event.target.classList.contains('draggable')) {        
          this.isPress = true;                
      }
      if(event.target.classList.contains('cur-areas__rect-drag-right')) {
          this.direction = true;
      }
      if(event.target.classList.contains('cur-areas__rect-drag-left')) {
          this.direction = false;
      }
    });

    this.svg.addEventListener('mouseup', () => {
        this.isPress = false;
    });

    this.svg.addEventListener('mousemove', (event) => {
        if(this.isPress) {
            this.drag(event, null);                          
        }
    });

    this.$emit('isGraphRender', this.isSave);

  },

  watch: {
    hoveredStatus(val, oldVal) {
      if(!this.isGraphEdit) this.drawCurArea(val, oldVal);
    },

    curIdx(val, oldVal) {
      this.drawCurArea(val, oldVal);
    },

    isGraphEdit(val) {
      if(val) {
          document.querySelector('.cur-areas__group').style.display = 'none';
          this.svg.style.pointerEvents = 'all';
          this.svg.append(this.getEditArea());
          logbook.getSvgElMiddlePosition(document.querySelector('.cur-areas__duration-txt'), document.querySelector('.cur-areas__duration'));

          this.editPathData.y = this.drivingStatus[this.editedStatus];
          this.editGraphData.y = this.drivingStatus[this.editedStatus];
          
          this.svg.append(this.getCopyPath());
        }            
        else {
            this.resetGraph();
        }
    },
    
    editPathData: {
        handler: function() {            
            if(this.isGraphEdit) {
                this.editPath();
          }
      },
      deep: true            
    },

    editedStatus(val) {
        this.editPathData.y = this.drivingStatus[val];            
        this.editGraphData.y = this.drivingStatus[val];
    },

    pickedTime(val) {
        if(val) {    
            this.curRight_X = logbook.getCoords(this.getGridLength, val.x2);
            this.curLeft_X = logbook.getCoords(this.getGridLength, val.x1);
            
            this.editGraphData.x1 = this.curLeft_X;
            this.editGraphData.x2 = this.curRight_X;
            
            if(this.isGraphEdit) {
                this.drag(null);
            }
        }
    },

    editedGraphArr(val) {
        if(val) {
            this.getUpdatedetStatusesTime;
        }
    },
  },

  computed: {
    getGridLength() {
      return (this.cols - 1) * this.cell;
    },

    getGridHeight() {
      return (this.rows - 1) * this.cell;
    },

    getDrivingStatus() {
      let start = this.cell/2;
      for(let i=0; i<this.statusLabels.length; i++) {
          this.drivingStatus[this.statusLabels[i].name] = start;
          start += this.cell;
      }
    },

    getCurPath() {
      let status = '',
          statusSeconds = 0,
          x = 0,
          y = 0;          

      if(this.graphData) {
          for(let i=0; i<this.graphData.length; i++) {
              status = this.graphData[i].status;
              statusSeconds = getSecondsFromHourse(this.graphData[i].dateTime, true),
              x = graph.getCoords(this.getGridLength, statusSeconds),
              y =  this.drivingStatus[status]; 

              this.curAreasCoords.push({
                  x: x,
                  y: y,
                  time: statusSeconds
              });                   
              
              if(i<=0) {
                  this.pathAttr += `${x} ${y}`;
              }
              else {
                  this.pathAttr += ` H${x}`;
                  this.pathAttr += ` V${y}`;

              // Filling out cycles total time fields (this.statusLabels arr)
                  for(let y=0; y<this.statusLabels.length; y++) {
                    if(this.statusLabels[y].name == this.graphData[i-1].status) {  
                      this.statusLabels[y].val += getSecondsFromHourse(this.graphData[i].dateTime, true) - getSecondsFromHourse(this.graphData[i-1].dateTime, true);
                    }
                  }
                }           
                
                if(i>=this.graphData.length-1) {
                  this.pathAttr += ` H${this.getGridLength} V${y}`;
                  
                  for(let y=0; y<this.statusLabels.length; y++) {
                    if(this.statusLabels[y].name == this.graphData[i].status) {   
                        this.statusLabels[y].val += this.secondsInDay - getSecondsFromHourse(this.graphData[i].dateTime);                                    
                    }
                }
              }
          }

      // Cycles total time
          for(let y = 0; y < this.statusLabels.length; y++) {
              this.totalStatusTime +=  this.statusLabels[y].val;
          }

          for(let i in this.statusLabels) {
              for(let key in this.statusLabels[i]) {
                  this.statusesForRender[i][key] = this.statusLabels[i][key];
              }         
          }

          this.pathAttr = 'M '+ this.pathAttr;          
          return this.pathAttr;
      }
    },

    getConvertedDuration() {
      return (val) => {
          return  (val >= this.secondsInDay) ? getHourseFromSeconds(val, 'kk:mm') : getHourseFromSeconds(val, 'HH:mm');
      }
    },

    getConvertedTotalTime() {
      return getHourseFromSeconds(this.totalStatusTime, 'kk:mm:ss');
    },

    getUpdatedetStatusesTime() {
        logbook.getStatusesDuration(this.editedGraphArr, this.secondsInDay);
        for(let i in this.statusesForRender) {
            this.statusesForRender[i].val = 0;
            for(let y in this.editedGraphArr) {
                if(this.statusesForRender[i].name == this.editedGraphArr[y].status) {                    
                    this.statusesForRender[i].val += this.editedGraphArr[y].duration;                    
                }
            }
        }
    },

  },

  methods: {
    getLine(x1, y1, x2, y2, className) {
      let line = document.createElementNS(this.ns, 'line');
      graph.setAttributes(line, {x1: x1, y1: y1, x2: x2, y2: y2, class: className});

      return line;
    },

    getGrid() {    
      let g = document.createElementNS(this.ns, 'g'),
          row= 0,
          col = 0,
          step = this.cell,
          attrs = {ns: this.ns, styles: this.common_styles};
          
      for(let i=0; i<this.rows; i++) {
          g.append(graph.getRow(attrs, this.getGridLength, row));
          row += step;
      }
      
      for(let i=0; i<this.cols; i++) {
          g.append(graph.getCols(attrs, this.getGridHeight, col));

          if(i<(this.cols - 1)) {
              g.appendChild(graph.getMinutes(attrs, col, 0, 8, this.gridStep));
              g.appendChild(graph.getMinutes(attrs, col, step, 8, this.gridStep));
              g.appendChild(graph.getMinutes(attrs, col, step*3, -8, -this.gridStep));
              g.appendChild(graph.getMinutes(attrs, col, step*4, -8, -this.gridStep));
          }
          col += step;
      }
              
        return g;
    },

    getCurAreasLines(x1, x2, idx) {
      let g = document.createElementNS(this.ns, 'g'),
          lineLeft = this.getLine(x1, 0, x1, this.getGridHeight, 'cur-areas__brd'),
          lineRight = this.getLine(x2, 0, x2, this.getGridHeight, 'cur-areas__brd'),
          rectX1 = x1 - this.timeRectWidth,
          rectY1 = 0 - (this.timeRectHeight+5),
          rectX2 = x2 + this.timeRectWidth,
          durationTime = 0,
          startTimeText = 0,
          endTimeText = 0,
          timeBlockParam = {},
          durationBlockParam = {};
          
      if(idx>=this.curAreasCoords.length-1) {
          durationTime = (this.curAreasCoords.length == 1)
                          ? getHourseFromSeconds((this.curAreasCoords[idx].time - this.curAreasCoords[idx].x), 'kk:mm')
                          : getHourseFromSeconds((this.secondsInDay - this.curAreasCoords[idx].time), 'HH:mm');
          if(this.curAreasCoords.length == 1) {
              rectX1 = x2;
          }
      }
      else {
          durationTime = getHourseFromSeconds((this.curAreasCoords[idx+1].time - this.curAreasCoords[idx].time), 'HH:mm');
      }           

      logbook.setAttributes(g, {id: idx, class: 'cur-areas__area', width: x2});

      startTimeText = getHourseFromSeconds(this.curAreasCoords[idx].time, 'HH:mm');            
      endTimeText = (idx>=this.curAreasCoords.length-1) ? getHourseFromSeconds(this.secondsInDay, 'HH:mm') : getHourseFromSeconds(this.curAreasCoords[idx+1].time, 'HH:mm');

      timeBlockParam = {ns: this.ns, x1: x1, x2: x2, rectX1: x1 - this.timeRectWidth, rectY1: rectY1, rectX2: rectX2, startTime: startTimeText, endTime: endTimeText};
      durationBlockParam = {ns: this.ns, x1: x1, x2: x2, rectX1: rectX1, rectX2: rectX2, gridLength: this.getGridLength, durationTime: durationTime};

      let timeBlock = logbook.getTimeBlock(timeBlockParam);
      let durationBlock = logbook.getDurationBlock(durationBlockParam);
      
      g.append(lineLeft);
      g.append(lineRight);
      
      for(let key in timeBlock) {
          g.append(timeBlock[key]);
      }
      
      for(let key in durationBlock) {
          g.append(durationBlock[key]);
      }
      
      return g;
    },

    drawCurArea(cur, old) {    
      if(cur == null) {
          logbook.setAttributes(this.curAreas[old], {class: ''});
          if(document.getElementById(old)) {
              document.getElementById(old).remove();
          }
          return
      }

      cur = Number(cur);

      if(this.curIdx && this.curIdx != cur) {
          logbook.setAttributes(this.curAreas[old], {class: ''});
          if(document.getElementById(this.curIdx)) {
              document.getElementById(this.curIdx).remove();                    
          }
      }

      if(this.hoveredStatus && this.hoveredStatus != cur) {
          logbook.setAttributes(this.curAreas[old], {class: ''});
          if(document.getElementById(this.hoveredStatus)) {
              document.getElementById(this.hoveredStatus).remove();
          }
      }
      
      let x1 = this.curAreasCoords[cur].x,
          x2 = (cur>=this.curAreasCoords.length-1) ? this.getGridLength : this.curAreasCoords[cur+1].x;
          logbook.setAttributes(this.curAreas[cur], {class: 'cur-areas__active-area'});
      
      this.curAreas[cur].before(this.getCurAreasLines(x1, x2, cur));
      
      if(old) {
          logbook.setAttributes(this.curAreas[old], {class: ''});
      }
      if(document.getElementById(old)) {
          document.getElementById(old).remove();
      }
      
      logbook.getSvgElMiddlePosition(document.querySelector('.cur-areas__duration-txt'), document.querySelector('.cur-areas__duration'));
    },

    showCurStatus(event) {
      if(event.target.dataset.idx) {
          this.curIdx = event.target.dataset.idx;
          this.$emit('curId', this.curIdx);
      }
    },

    getEditArea() {
      this.editedAreaWidth = this.graphStep;
      let g = document.createElementNS(this.ns, 'g'),
          x1 = 0,
          x2 = this.editedAreaWidth,
          durationTime = 0,
          rectX1 = x1 - this.timeRectWidth,
          rectX2 = x2+this.timeRectWidth,
          rectY1 = 0 - (this.timeRectHeight+5),
          
          rect = document.createElementNS(this.ns, 'rect'),
          lineLeft = this.getLine(x1, 0, x1, this.getGridHeight, 'cur-areas__brd cur-areas_left'),
          lineRight = this.getLine(x2, 0, x2, this.getGridHeight, 'cur-areas__brd cur-areas_right '),
          startTimeText = 0,
          endTimeText = 0,
          timeBlockParam = {},
          durationBlockParam = {},
          draggableBlockParam = {},
          timeBlock = null,
          durationBlock = null,
          draggableBlock = null;
      
      logbook.setAttributes(g, {class: 'edited-area'});
      logbook.setAttributes(rect, {x: 0, y:0, width: this.graphStep, height: this.getGridHeight, class: 'duration-rect cur-areas__active-area'});
  
      startTimeText = getHourseFromSeconds(logbook.getSecFromPx(this.getGridLength, startTimeText), 'HH:mm');            
      endTimeText = getHourseFromSeconds(logbook.getSecFromPx(this.getGridLength, this.editedAreaWidth), 'HH:mm');
      durationTime = getHourseFromSeconds(logbook.getSecFromPx(this.getGridLength, x2) - logbook.getSecFromPx(this.getGridLength, x1), 'HH:mm'); 

      timeBlockParam = {ns: this.ns, x1: x1, x2: x2, rectX1: rectX1, rectY1: rectY1, rectX2: rectX2, startTime: startTimeText, endTime: endTimeText};
      durationBlockParam = {ns: this.ns, x1: x1, x2: x2, rectX1: rectX1, rectX2: rectX2, gridLength: this.getGridLength, durationTime: durationTime};
      draggableBlockParam = {ns: this.ns, x1: x1, x2: x2, y: this.getGridHeight};

      timeBlock = logbook.getTimeBlock(timeBlockParam);
      durationBlock = logbook.getDurationBlock(durationBlockParam);
      draggableBlock = logbook.getDraggableBlock(draggableBlockParam);

      g.append(rect);
      g.append(lineLeft);
      g.append(lineRight);
      
      for(let key in timeBlock) {
          g.append(timeBlock[key]);
      }
      
      for(let key in durationBlock) {
          g.append(durationBlock[key]);
      }
      
      for(let key in draggableBlock) {
          g.append(draggableBlock[key]);
      }

      return g;
    },

    getCopyPath() {
      let path = document.createElementNS(this.ns, 'path');

      let firstPoint = `M 0 15 H7.5 V${this.drivingStatus[this.graphData[0].status]}`;

      let res = '';
      
      this.pathAttr.split(" H").map((item, index) => {
        if(index <= 0) {
            res = firstPoint;
        }
        if(index > 0) {
            res += ` H${item}`;
        }
        return res;

      });
    //   logbook.setAttributes(path, {d: this.pathAttr, id: 'edited-path'});
      logbook.setAttributes(path, {d: res, id: 'edited-path'});
      return path;
    },

    resetGraph() {
      this.svg.style.pointerEvents = 'none';
      document.querySelector('.cur-areas__group').style.display = 'block';
      if(document.querySelector('.edited-area')) document.querySelector('.edited-area').remove();            
      if(document.querySelector('#edited-path')) document.querySelector('#edited-path').remove();
      this.curRight_X = this.graphStep;
      this.curLeft_X = 0;
      this.direction = true;
      this.editPathData = {
          x1:0, x2: this.graphStep, y: 0
      };

      for(let i in this.statusLabels) {
          for(let key in this.statusLabels[i]) {
              this.statusesForRender[i][key] = this.statusLabels[i][key];
          }          
      }
    },

    drag(event) {
      let pt = this.svg.createSVGPoint(),
          durationFormat = logbook.getSecFromPx(this.getGridLength, this.curRight_X - this.curLeft_X) >= this.secondsInDay ? 'kk:mm' : 'HH:mm',
          endTimeTextFormat = logbook.getSecFromPx(this.getGridLength, this.curRight_X) >= this.secondsInDay ? 'kk:mm' : 'HH:mm',
          durationTimeText = getHourseFromSeconds(logbook.getSecFromPx(this.getGridLength, this.curRight_X - this.curLeft_X), durationFormat),
          endTimeText = getHourseFromSeconds(logbook.getSecFromPx(this.getGridLength, this.curRight_X), endTimeTextFormat),
          startTimeText = getHourseFromSeconds(logbook.getSecFromPx(this.getGridLength, this.curLeft_X), 'HH:mm');

      if(event) {
          pt.x = event.clientX;
          pt = pt.matrixTransform(this.svg.getScreenCTM().inverse());
          
          if(this.curRight_X - this.curLeft_X >= this.graphStep) {
              document.querySelector('.cur-areas__rect-drag-left').classList.add('draggable');
          }

          if(this.direction) { // drag right side
              if(pt.x > this.curRight_X || (this.curRight_X - this.curLeft_X <= this.graphStep)) {
                  this.curRight_X += this.graphStep;
              }
              if(pt.x < this.curRight_X) {
                  this.curRight_X -= this.graphStep;                        
              }                
          }
          else { // drag left side
              if(pt.x > this.curLeft_X) {
                  this.curLeft_X += this.graphStep;
              }
              if(pt.x < this.curLeft_X || this.curLeft_X >= this.curRight_X) {
                  this.curLeft_X -= this.graphStep;
              }
          }
                          
      }

      this.curRight_X = (this.curRight_X >= this.getGridLength) ? this.getGridLength : this.curRight_X;
      this.curLeft_X = this.curLeft_X <= 0 ? 0 : this.curLeft_X;
      this.graphEditedAreaWidth = this.curRight_X - this.curLeft_X;

      logbook.setAttributes(document.querySelector('.duration-rect'), {x: this.curLeft_X, width: this.graphEditedAreaWidth});

      logbook.setAttributes(document.querySelector('.cur-areas_right'), {x1: this.curRight_X, x2: this.curRight_X}); //moove line right            
      logbook.setAttributes(document.querySelector('.cur-areas_left'), {x1: this.curLeft_X, x2: this.curLeft_X}); //moove line left            

      logbook.updateEndTimeBlockAttributes(document.querySelector('.cur-areas_end-time'), 
                                           document.querySelector('.cur-areas_end-time-txt'), 
                                           this.curRight_X, endTimeText, this.graphStep); // moove EndTimeBlock

      logbook.updateStartTimeBlockAttributes(document.querySelector('.cur-areas_start-time'), 
                                             document.querySelector('.cur-areas_start-time-txt'), 
                                             this.curLeft_X, startTimeText); // moove StartTimeBlock

      logbook.updateDurationAttributes(document.querySelector('.cur-areas__duration'), 
                                       document.querySelector('.cur-areas__duration-txt'), 
                                       durationTimeText, {x1: this.curLeft_X, x2: this.curRight_X});  // moove DurationTimeBlock

      logbook.updatedDagRightElAttributes(document.querySelector('.cur-areas__rect-drag-right'), this.curRight_X); // moove RightDragBlock
      logbook.updatedDagLeftElAttributes(document.querySelector('.cur-areas__rect-drag-left'), this.curLeft_X); // moove LeftDragBlock
                  
      this.editPathData.x1 = this.curLeft_X;
      this.editPathData.x2 = this.curRight_X;

      this.$emit('setTimeInterval', {x1: logbook.getSecFromPx(this.getGridLength, this.curLeft_X), x2: logbook.getSecFromPx(this.getGridLength, this.curRight_X)});
    },

    editPath() {     
      let el = document.querySelector('#edited-path'),
          attr = '',
          firstPoint = '',
          rightPos = '',
          leftPos = '',
          restStr = '',
          initialArr = [...this.curAreasCoords];

      if(initialArr[initialArr.length-1].x < this.getGridLength) {                
          initialArr[initialArr.length] = {
              x: this.getGridLength,
              y: initialArr[initialArr.length-1].y,
              time: initialArr[initialArr.length-1].time
          }
      }

      this.curArrItemRightIdx = initialArr.findIndex((item) => {
          if(item.x >= this.editPathData.x2) {
              return item
          }
      });

      this.curArrItemLeftIdx = initialArr.findIndex((item) => {
          if(item.x > this.editPathData.x1) {
              return item
          }
      });

      this.curArrItemY = initialArr[this.curArrItemLeftIdx-1].y;            

      this.curArrItemRightIdx = this.curArrItemRightIdx < 0 ? initialArr.length-1 : this.curArrItemRightIdx-1;
      this.curArrItemLeftIdx = this.curArrItemLeftIdx < 0 ? initialArr.length-1 : this.curArrItemLeftIdx-1;
      
      attr = this.pathAttr.split(" H").map((item, index) => {
          let res = '';
          if(index <= 0) {
              res += item;
          }
          if(index > 0) {
              res += `H${item}`;
          }
          return res;
      });
      
      firstPoint = this.updateString(attr[0], this.editPathData.y);
                  
      if((this.curArrItemRightIdx) <= 0) {
          restStr = attr.slice(this.curArrItemRightIdx+1).join(" ");
      }
      if(this.editPathData.x2 > 0) {

          if(this.editPathData.x1 <= 0) {
              rightPos = `${firstPoint} H${this.editPathData.x2} V${initialArr[this.curArrItemRightIdx].y}`;
          }
          if(this.editPathData.x1 > 0) {
              rightPos = `H${this.editPathData.x2} V${initialArr[this.curArrItemRightIdx].y}`;
          }
          if(this.editPathData.x2 == initialArr[this.curArrItemRightIdx+1].x && this.editPathData.x1 <= 0) {
              rightPos = `${firstPoint}`;
          }               
      }
      if(initialArr[this.curArrItemRightIdx+1].x > this.editPathData.x2 && this.curArrItemRightIdx > 0) {
          restStr = attr.slice(this.curArrItemRightIdx+1).join(" ");
      }
      if(initialArr[this.curArrItemRightIdx+1].x == this.editPathData.x2 && this.curArrItemRightIdx > 0) { 

          rightPos = (this.editPathData.x1 <= 0) ? `${firstPoint}` : '';

          restStr = `${attr.slice(this.curArrItemRightIdx+1).join(" ")}`;
      }
      if(this.editPathData.x2 >= this.getGridLength) {
          rightPos = (this.editPathData.x1 <= 0)
                      ? `${firstPoint} H${this.editPathData.x2} V${this.editPathData.y}`
                      : `H${this.editPathData.x2} V${this.editPathData.y}`;
          restStr = '';
      }
      if(this.editPathData.x1 > 0) {
          let idx = 0;
          
          if(this.editPathData.x1 > initialArr[this.curArrItemLeftIdx].x) {
              idx = this.curArrItemLeftIdx+1;
              
          }
          if(initialArr[this.curArrItemLeftIdx].x == this.editPathData.x1) {
              idx = this.curArrItemLeftIdx;
          }

          if(initialArr.length <= 2) {
              idx = this.curArrItemLeftIdx+1;
          }
          
          firstPoint = `${attr.slice(0, idx).join(" ")}`;
          leftPos = `${firstPoint} H${this.editPathData.x1} V${this.editPathData.y}`;
      }

      
      attr = `${leftPos} ${rightPos} ${restStr}`;

      this.getEditedPathArr(attr);
      
      if(el) logbook.setAttributes(el, {d: attr});
    },

    updateString(str, y, pos) {
      let temp_arr =  str.split(" ");
      temp_arr[temp_arr.length-1] = pos ? pos+y : y;
      return temp_arr.join(" ");
    },

    getEditedPathArr(path) {
      let arr = path.split(" H").map((item, index) => {
          let res = "";
          if(index<=0) {
              res = item.trim().replace(/M\s/, '');
          }
          else {
              res = item.replace(/V/, '');
          }
          return res;
      }),
        statuses = [];            

      for(let i=0; i<arr.length-1; i++) {
          let curStatus = '';

          for(let key in this.drivingStatus) {
              if(this.drivingStatus[key] == Number(arr[i].split(" ")[1])) {
                  curStatus = key;
              }
          }
          
          statuses[i] = {
              created_at: logbook.getSecFromPx(this.getGridLength, arr[i].split(" ")[0]),
              status: curStatus,
              duration: 0,
          }          
          
          this.editedGraphArr = [...statuses];
          this.$emit('curStatusList', statuses);           
      }
    },
    
  }
}
